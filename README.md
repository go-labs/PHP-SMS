# PHP-SMS

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

This library is used to send SMS programmatically through a GSM (Global System for Mobile Communications).

## Install

Via Composer

``` bash
$ composer require go-labs/sms
```

## Usage
You have to add these lines into your PHP code.

``` php
 use GoLabs\Sms\Sms;
 ----
 $sms = new Sms($serialPort, $baudRate, $parity, $lenght, $stopBits, $flowControl);
 $sms->sendMessage($countryCode, $phoneNumber, $message);
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email jvargas@go-labs.net instead of using the issue tracker.

## Credits

- [GoLabs][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/GoLabs/PHP-SMS.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/GoLabs/PHP-SMS/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/GoLabs/PHP-SMS.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/GoLabs/PHP-SMS.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/GoLabs/PHP-SMS.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/GoLabs/PHP-SMS
[link-travis]: https://travis-ci.org/GoLabs/PHP-SMS
[link-scrutinizer]: https://scrutinizer-ci.com/g/GoLabs/PHP-SMS/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/GoLabs/PHP-SMS
[link-downloads]: https://packagist.org/packages/GoLabs/PHP-SMS
[link-author]: https://github.com/go-labs
