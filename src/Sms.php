<?php

namespace GoLabs\Sms;

class Sms
{
    private $serial = null;

    public function __construct($serialPort, $baudRate, $parity, $lenght, $stopBits, $flowControl)
    {
        $this->serial = new PhpSerial();
        $this->serial->deviceSet($serialPort);
        $this->serial->confBaudRate($baudRate);
        $this->serial->confParity($parity);
        $this->serial->confCharacterLength($lenght);
        $this->serial->confStopBits($stopBits);
        $this->serial->confFlowControl($flowControl);
    }

    public function openDevice()
    {
        $this->serial->deviceOpen();
    }

    public function closeDevice()
    {
        $this->serial->deviceClose();
    }

    public function sendMessage($countryCode, $phoneNumber, $message)
    {
        $this->openDevice();
        $this->serial->sendMessage("AT+CMGF=1\n\r");
        $this->serial->sendMessage('AT+CMGS="+'.$countryCode.$phoneNumber."\"\n\r");
        $this->serial->sendMessage($message."\n\r");
        $this->serial->sendMessage(chr(26));
        $this->closeDevice();
    }

    public function getDevice()
    {
        return $this->serial->getDevice();
    }

    public function getWinDevice()
    {
        return $this->serial->getWinDevice();
    }

    public function getOS()
    {
        return $this->serial->getOS();
    }

    public function getHandle()
    {
        return $this->serial->getHandle();
    }

    public function getState()
    {
        return $this->serial->getState();
    }
}
