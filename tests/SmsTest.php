<?php

namespace GoLabs\Test;

use GoLabs\Sms;

class SmsTest extends \PHPUnit_Framework_TestCase
{
    private $serialPort;
    private $baudRate;
    private $parity;
    private $lenght;
    private $stopBits;
    private $flowControl;
    private $sms;

    public function setUp()
    {
        $this->serialPort = "COM20";
        $this->baudRate = 115200;
        $this->parity = "none";
        $this->lenght = 8;
        $this->stopBits = 1;
        $this->flowControl = "xon/xoff";
        $this->sms = new Sms($this->serialPort, $this->baudRate, $this->parity, $this->lenght, $this->stopBits, $this->flowControl);
    }

    public function testNotNullProperties()
    {
        $this->assertNotNull($this->sms->getDevice());
        $this->assertNotNull($this->sms->getWinDevice());
        $this->assertNotNull($this->sms->getOS());
    }

    public function testValidProperties()
    {
        $this->assertEquals("windows", $this->sms->getOS());
        $this->assertEquals("\\\.\COM11", $this->sms->getDevice());
        $this->assertEquals("COM11", $this->sms->getWinDevice());
    }

    public function testConnectionOpened()
    {
        $this->sms->openDevice();
        $this->assertNotNull($this->sms->getHandle());
        $this->assertEquals(2, $this->sms->getState());
        $this->sms->closeDevice();
    }

    public function testConnectionClosed()
    {
        $this->sms->openDevice();
        $this->sms->closeDevice();
        $this->assertNull($this->sms->getHandle());
        $this->assertEquals(1, $this->sms->getState());
    }
}
